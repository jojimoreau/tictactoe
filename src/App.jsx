import { useState, useEffect } from 'react'
import './App.scss'


export function checkTriplet(grid, a, b, c) {
  if(grid[a] !== "" && grid[a] === grid[b] && grid[b] === grid[c]){
    return true;
  }
  return false;
};

export function checkLines(grid) {
  return checkTriplet(grid, 0, 1, 2) || checkTriplet(grid, 3, 4, 5) || checkTriplet(grid, 6, 7, 8);
};

export function checkColumns(grid) {
  return checkTriplet(grid, 0, 3, 6) || checkTriplet(grid, 1, 4, 7) || checkTriplet(grid, 2, 5, 8);
};

export function checkDiagonals(grid) {
  return checkTriplet(grid, 0, 4, 8) || checkTriplet(grid, 6, 4, 2);
};

export function checkGrid(grid) {
  return checkLines(grid) || checkColumns(grid) || checkDiagonals(grid);
};

const initialState = {
  message: "",
  circleTurn: false,
  grid: [
    "", "", "",
    "", "", "",
    "", "", ""
  ],
  turnCount: 0,
}

function App() {
  const [message, setMessage] = useState(initialState.message);
  const [circleTurn, setCircleTurn] = useState(initialState.circleTurn);
  const [grid, setGrid] = useState(initialState.grid);
  const [turnCount, setTurnCount] = useState(initialState.turnCount);

  const addSignInGrid = (position, value) => {
    let nextGrid = [...grid];
    nextGrid[position] = value;
    setGrid(nextGrid);
  };

  const cellAlreadyAllocated = (grid, position) => {
    if(grid[position]){
      return true;
    }
    return false;
  };

  const handleCellClick = (e, position) => {
    e.preventDefault();
    if(cellAlreadyAllocated(grid, position)){
      return;
    }    
    setTurnCount(turnCount + 1);    
    addSignInGrid(position, circleTurn ? "circle" : "cross");  
    setCircleTurn(!circleTurn);
  };

  const resetGame = () => {
    setMessage(initialState.message);
    setGrid(initialState.grid);
    setTurnCount(initialState.turnCount);
    setCircleTurn(initialState.circleTurn);
  };

  /* When turnCount changes value */
  useEffect(() => {
    if(turnCount <= 9 && checkGrid(grid)){
      setMessage((!circleTurn ? "Circle" : "Cross") + " won !");
    }
    else if(turnCount >= 9){
      setMessage("Draw !");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [turnCount, grid]);

  const gridSetup = [
    "cell top left", "cell top", "cell top right",
    "cell mid left", "cell mid", "cell mid right",
    "cell bottom left", "cell bottom", "cell bottom right"
  ];

  return (
    <div className="app">
      <h1>Tic Tac Toe</h1>
      <div className='header' style={turnCount === 0 ? {visibility: "hidden"} : {}}>
        <a href="#reset" onClick={resetGame}>Reset</a>
      </div>  
      <div className={`grid ${circleTurn ? "circle_turn" : "cross_turn"} ${message ? "disabled" : ""}`} >
        {
          message &&
          <div className="message">
            <p>{ message }</p>
            <a href="#reset" onClick={resetGame} data-testid={"reset_button"}>Retry</a>
          </div>
        }        
        {
          gridSetup.map((el, index) => {
            return <div key={"cell" + index} data-testid={"cell_"+index} className={el + " " + grid[index]} onClick={(e) => handleCellClick(e, index)} />
          })
        }
      </div>
      <div className='footer'>
        <a href="coverage/index.html">Test coverage</a>
        <a href="https://gitlab.com/jojimoreau/tictactoe">Gitlab repo</a>
      </div>    
    </div>
  );
}

export default App
