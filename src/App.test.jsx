// Imports
import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { describe, it, expect, test, beforeEach } from 'vitest'

// To Test
import App, { checkGrid } from './App';


beforeEach(async () =>{
    document.body.innerHTML = "";
});


// Tests
test('Renders main page correctly - Snapshot', () => {
    let result = render(<App />);
    expect(result).toMatchSnapshot()
});

describe('Detect wins correctly', () => {
    it('Check Diagonal - Circles wins', () => {
        let grid = [
            "circle"    , "cross"   , "",
            "cross"     , "circle"  , "",
            ""          , ""        , "circle"
        ];
        expect(checkGrid(grid)).toBe(true);
        grid = [
            ""          , "cross"   , "circle",
            "cross"     , "circle"  , "",
            "circle"    , ""        , ""
        ];
        expect(checkGrid(grid)).toBe(true);
    })

    it('Check Diagonal - Cross wins', () => {
        let grid = [
            "cross"     , "circle"  , "",
            "circle"    , "cross"   , "",
            ""          , ""        , "cross"
        ];
        expect(checkGrid(grid)).toBe(true);
        grid = [
            ""          , "circle"  , "cross",
            "circle"    , "cross"   , "",
            "cross"     , ""        , ""
        ];
        expect(checkGrid(grid)).toBe(true);
    })

    it('Check Diagonal - Circle wins', () => {
        let grid = [
            "circle"    , "cross"   , "",
            "cross"     , "circle"  , "",
            ""          , ""        , "circle"
        ];
        expect(checkGrid(grid)).toBe(true);
    })
})


describe('Detect no wins correctly', () => {
    it('when there is no winner - 1', () => {
        let grid = [
            "circle"    , "cross"   , "",
            "cross"     , "cross"   , "circle",
            ""          , ""        , "circle"
        ];
        expect(checkGrid(grid)).toBe(false);
    })

    it('when there is no winner - 2', () => {
        let grid = [
            "cross"     , "circle"  , "",
            "circle"    , "circle"  , "cross",
            ""          , ""        , "cross"
        ];
        expect(checkGrid(grid)).toBe(false);
    })

    it('when there is no winner - 3', () => {
        let grid = [
            "cross"    , "cross"   , "",
            "cross"    , "circle"  , "",
            "circle"   , ""        , "circle"
        ];
        expect(checkGrid(grid)).toBe(false);
    })
})


describe('Simulate normal game', () => {
    it('where Circle wins and click on reset link closes popup', async () => {
        let result = render(<App />);
        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;

        await userEvent.click(screen.queryByTestId('cell_8')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_0')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_1')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_3')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_2')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_6')); // Circle plays and wins

        // Message screen shown
        expect(screen.queryByTestId('reset_button')).to.exist;
        expect(result).toMatchSnapshot();

        // Click reset
        await userEvent.click(screen.queryByTestId('reset_button')); // Reset clicked

        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;
        expect(result).toMatchSnapshot();
    })

    it('where user clicks twice on a cell but game continues', async () => {
        render(<App />);
        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;

        await userEvent.click(screen.queryByTestId('cell_0')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_0')); // Circle plays - Nothing happend
        await userEvent.click(screen.queryByTestId('cell_1')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_3')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_2')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_6')); // Cross plays and wins
 
        // Message screen shown + check message
        expect(screen.queryByTestId('reset_button')).to.exist;
        expect(screen.queryByText('Cross won !')).to.exist;

        // Click reset
        await userEvent.click(screen.queryByTestId('reset_button')); // Reset clicked

        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;
    })

    it('where Nobody wins', async () => {
        render(<App />);
        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;

        await userEvent.click(screen.queryByTestId('cell_1')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_0')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_2')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_3')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_5')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_7')); // Circle plays and nobody wins
        
        // Message screen shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;
    })

    it('where Cross wins at last round', async () => {
        render(<App />);
        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;

        await userEvent.click(screen.queryByTestId('cell_1')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_0')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_2')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_3')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_5')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_7')); // Circle plays and nobody wins
        
        // Message screen shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;

        await userEvent.click(screen.queryByTestId('cell_6')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_4')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_8')); // Cross plays and wins

        expect(screen.queryByTestId('reset_button')).to.exist;
        expect(screen.queryByText('Cross won !')).to.exist;
    })

    it('where players plays till last round and gets a draw', async () => {
        render(<App />);
        // Message screen not shown
        expect(screen.queryByTestId('reset_button')).not.to.exist;

        await userEvent.click(screen.queryByTestId('cell_0')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_1')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_3')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_4')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_7')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_6')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_2')); // Cross plays
        await userEvent.click(screen.queryByTestId('cell_5')); // Circle plays
        await userEvent.click(screen.queryByTestId('cell_8')); // Cross plays and nobody wins

        expect(screen.queryByTestId('reset_button')).to.exist;
        expect(screen.queryByText('Draw !')).to.exist;
    })
})