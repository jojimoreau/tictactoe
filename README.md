# Tic Tac Toe Project

![Pipeline](https://gitlab.com/klintman/tictactoe/badges/main/pipeline.svg)


This project is a small project for a BNPPF interview.

## Stack

- NodeJS
- ReactJS
- ViteJs (Build tool)
- Vitest (Test framework)

## Installation

Use the package manager to install all dependencies.

```bash
npm i
```

## Usage

### Developement:

```javascript
npm run dev
```

Then browse your localhost server (Check console for the exact url)

### Tests:

You can run the tests with:

```javascript
npm run test
```

You can also generate the coverage report:

```javascript
npm run coverage
```

The coverage report will be in `/coverage/index.html`

After each commit, available on: [Test Coverage](https://klintman.gitlab.io/tictactoe/coverage/index.html)

### Build:

```javascript
npm run build
```

The build will be in `/dist/`

If you want to preview the build

```javascript
npm run preview
```

